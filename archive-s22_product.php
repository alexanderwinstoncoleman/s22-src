<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

get_header();

wp_rig()->print_styles( 'wp-rig-content' );

?>
	<main id="primary" class="site-main">
		<?php
		// $bbb  = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		// $args = array(
		// 	'post_type'      => 's22_product',
		// 	'posts_per_page' => 100,
		// 	'paged'          => $bbb,
		// );
		// $q    = new \WP_Query( $args );
		if ( have_posts() ) {

			get_template_part( 'template-parts/content/page_header' );
			?>
			<div class="grid-container archive-grid">
			<?php

			while ( have_posts() ) {
				the_post();

				get_template_part( 'template-parts/content/entry', get_post_type() );
			}

			get_template_part( 'template-parts/content/pagination' );
		} else {
			?>
			<div class="grid-container">
			<?php
			get_template_part( 'template-parts/content/error' );
		}
		?>
		</div><!-- .grid-container -->
	</main><!-- #primary -->
<?php
get_footer();
