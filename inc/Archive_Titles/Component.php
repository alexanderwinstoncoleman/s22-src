<?php
/**
 * WP_Rig\WP_Rig\Post_Thumbnails\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Archive_Titles;

use WP_Rig\WP_Rig\Component_Interface;
use function add_filter;

/**
 * Class for managing post thumbnail support.
 *
 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
 */
class Component implements Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'archive_titles';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_filter( 'get_the_archive_title', [ $this, 'archive_title_filter' ] );
	}

	/**
	 * Adds custom image sizes.
	 */
	public function archive_title_filter() {
		if ( is_category() || is_archive() ) {

			$title = single_cat_title( '', false );

		} elseif ( is_tag() ) {

			$title = single_tag_title( '', false );

		} elseif ( is_author() ) {

			$title = '<span class="vcard">' . get_the_author() . '</span>' ;

		}

		return $title;
	}
}
