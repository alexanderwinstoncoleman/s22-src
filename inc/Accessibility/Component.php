<?php
/**
 * WP_Rig\WP_Rig\Accessibility\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Accessibility;

use WP_Rig\WP_Rig\Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use WP_Post;
use function add_action;
use function add_filter;
use function wp_enqueue_script;
use function get_theme_file_uri;
use function get_theme_file_path;
use function wp_script_add_data;
use function wp_localize_script;
use function login_enqueue_scripts;
use function login_headerurl;

/**
 * Class for improving accessibility among various core features.
 */
class Component implements Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'accessibility';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_navigation_script' ] );
		add_action( 'login_enqueue_scripts', [ $this, 'load_login_page_styles' ] );
		add_action( 'login_headerurl', [ $this, 'change_login_url' ] );
		add_action( 'wp_print_footer_scripts', [ $this, 'action_print_skip_link_focus_fix' ] );
		add_filter( 'nav_menu_link_attributes', [ $this, 'filter_nav_menu_link_attributes_aria_current' ], 10, 2 );
		add_filter( 'page_menu_link_attributes', [ $this, 'filter_nav_menu_link_attributes_aria_current' ], 10, 2 );
	}

	/**
	 * Enqueues a script that improves navigation menu accessibility.
	 */
	public function action_enqueue_navigation_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}

		// Enqueue the navigation script.
//		wp_enqueue_script(
//			'wp-rig-navigation',
//			get_theme_file_uri( '/assets/js/navigation.min.js' ),
//			[],
//			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/navigation.min.js' ) ),
//			false
//		);
		// wp_enqueue_script(
		// 	'wp-rig-intersection-observer-polyfill',
		// 	get_theme_file_uri( '/assets/js/intersection-observer-polyfill.min.js' ),
		// 	[],
		// 	wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/intersection-observer-polyfill.min.js' ) ),
		// 	true
		// );
		// wp_script_add_data( 'wp-rig-intersection-observer-polyfill', 'async', true );
		// wp_script_add_data( 'wp-rig-intersection-observer-polyfill', 'precache', true );
		wp_enqueue_script(
			'wp-rig-modernizr-navigation',
			get_theme_file_uri( '/assets/js/modernizr.min.js' ),
			[],
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/modernizr.min.js' ) ),
			true
		);
		wp_enqueue_script(
			'wp-rig-classie-navigation',
			get_theme_file_uri( '/assets/js/classie.min.js' ),
			array( 'wp-rig-modernizr-navigation' ),
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/classie.min.js' ) ),
			true
		);
		wp_enqueue_script(
			'wp-rig-push-navigation',
			get_theme_file_uri( '/assets/js/pushmenu.min.js' ),
			// array( 'wp-rig-classie-navigation', 'wp-rig-modernizr-navigation', 'wp-rig-intersection-observer-polyfill' ),
			array( 'wp-rig-classie-navigation', 'wp-rig-modernizr-navigation' ),
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/pushmenu.min.js' ) ),
			// wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/src/pushmenu.js' ) ),
			true
		);
		wp_script_add_data( 'wp-rig-push-navigation', 'defer', true );
		wp_script_add_data( 'wp-rig-push-navigation', 'precache', true );
		wp_localize_script(
			'wp-rig-navigation',
			'wpRigScreenReaderText',
			[
				'expand'   => __( 'Expand child menu', 'wp-rig' ),
				'collapse' => __( 'Collapse child menu', 'wp-rig' ),
			]
		);

		// DEQUEING wp-embed.min.js
		wp_deregister_script( 'wp-embed' );

		if ( ! is_user_logged_in() ) {
			wp_enqueue_script(
				's22-gtag',
				'https://www.googletagmanager.com/gtag/js?id=UA-153100838-1',
				array(),
				false,
				true
			);
			wp_enqueue_script(
				's22-analytics',
				get_theme_file_uri( '/assets/js/analytics.min.js' ),
				array('s22-gtag'),
				false,
				true
			);
			wp_script_add_data( 's22-gtag', 'async', true );
			wp_script_add_data( 's22-gtag', 'precache', true );
			wp_script_add_data( 's22-analytics', 'async', true );
			wp_script_add_data( 's22-analytics', 'precache', true );
		}

		// ARRAY OF HANDLES TO DEQUEUE
		$dequeue_styles_by_handle = array(
			'sb_instagram_styles'
		);
		foreach ($dequeue_styles_by_handle as $handle ) {
			# code...
			wp_dequeue_style( $handle );
		}

		// ENQUEUE INSTAGRAM ONLY ON FRONT PAGE
		if ( is_front_page() ) {
			wp_enqueue_style( 'sb_instagram_styles' );
		}
	}

	/**
	 * Prints an inline script to fix skip link focus in IE11.
	 *
	 * The script is not enqueued because it is tiny and because it is only for IE11,
	 * thus it does not warrant having an entire dedicated blocking script being loaded.
	 *
	 * Since it will never need to be changed, it is simply printed in its minified version.
	 *
	 * @link https://git.io/vWdr2
	 */
	public function action_print_skip_link_focus_fix() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}

		// Print the minified script.
		?>
		<script>
		/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
		</script>
		<?php
	}

	/**
	 * Filters the HTML attributes applied to a menu item's anchor element.
	 *
	 * Checks if the menu item is the current menu item and adds the aria "current" attribute.
	 *
	 * @param array   $atts The HTML attributes applied to the menu item's `<a>` element.
	 * @param WP_Post $item The current menu item.
	 * @return array Modified HTML attributes
	 */
	public function filter_nav_menu_link_attributes_aria_current( array $atts, WP_Post $item ) : array {
		if ( isset( $item->current ) ) {
			if ( $item->current ) {
				$atts['aria-current'] = 'page';
			}
		} elseif ( ! empty( $item->ID ) ) {
			global $post;

			if ( ! empty( $post->ID ) && (int) $post->ID === (int) $item->ID ) {
				$atts['aria-current'] = 'page';
			}
		}

		return $atts;
	}

	public function load_login_page_styles() {
		wp_enqueue_style( 's22-login-styles', get_theme_file_uri( '/assets/css/s22-login.min.css' ) );
	}

	public function change_login_url( $url ) {
		$home = get_bloginfo( 'url' );
		return $home;
	}
}
