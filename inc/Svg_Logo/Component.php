<?php
/**
 * WP_Rig\WP_Rig\Svg_Logo\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Svg_Logo;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;

/**
 * Class for SVG Logo.
 *
 * Exposes template tags:
 * * `wp_rig()->display_svg_logo()`
 *
 * @link https://wordpress.org/plugins/amp/
 */
class Component implements Component_Interface, Templating_Component_Interface {
    private $svg_file;
    private $home_url;
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'svg_logo';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
//		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_comment_reply_script' ] );
	}

	/**
	 * Gets template tags to expose as methods on the Template_Tags class instance, accessible through `wp_rig()`.
	 *
	 * @return array Associative array of $method_name => $callback_info pairs. Each $callback_info must either be
	 *               a callable or an array with key 'callable'. This approach is used to reserve the possibility of
	 *               adding support for further arguments in the future.
	 */
	public function template_tags() : array {
		return [
			'display_svg_logo' => [ $this, 'display_svg_logo' ],
		];
	}

	public function display_svg_logo(){
        $this->svg_file = get_stylesheet_directory_uri() . '/assets/images/svg-logo.svg';
        $this->home_url = get_bloginfo( 'url' );

        echo '<a href="' . $this->home_url . '"><img src="' . $this->svg_file . '" width="200" alt="suite 22 contract logo"></a>';
    }
}
