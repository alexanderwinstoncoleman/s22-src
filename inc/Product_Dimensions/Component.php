<?php
/**
 * WP_Rig\WP_Rig\Product_Dimensions\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Product_Dimensions;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;

/**
 * Class for managing product dimensions.
 *
 * @link
 */

class Component implements Component_Interface, Templating_Component_Interface {
	// public $height;
	// public $width;
	// public $depth;
	// public $seat_height;
	// public $dimensions = [];
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'product_dimensions';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_slick_slider_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}
	}

	public function get_product_dimensions() {
		// $this->height                    = get_field( 'height', the_ID() );
		// $this->width                     = get_field( 'width' );
		// $this->depth                     = get_field( 'depth' );
		// $this->seat_height               = get_field( 'seat_height' );
		// $this->dimensions['height']      = get_field( 'height' );
		// $this->dimensions['width']       = get_field( 'width' );
		// $this->dimensions['depth']       = get_field( 'depth' );
		// $this->dimensions['seat_height'] = get_field( 'seat_height' );
	}

	public function display_product_dimensions() {
		$height      = get_field( 'height' );
		$width       = get_field( 'width' );
		$depth       = get_field( 'depth' );
		$seat_height = get_field( 'seat_height' );
		$op         .= '<div class="product-dimensions">';
		$op         .= '<h2>Dimensions:</h2>';
		if ( null == $height && null == $width && null == $depth && null == $seat_height ) {
			$op .= 'n/a';
		} else {
			$dimensions['Height']      = $height;
			$dimensions['Width']       = $width;
			$dimensions['Depth']       = $depth;
			$dimensions['Seat Height'] = $seat_height;
			$unit                      = '"';
			$op                       .= '<ul class="no-padding">';
			foreach ( $dimensions as $dimension => $value ) {
				if ( null == $value || empty( $value ) ) {
					continue;
				} else {
					$op .= '<li>' . $dimension . ': ' . $value . $unit . '</li>';
				}
			}
			$op .= '</ul>';
		}
		$op .= '</div><!-- .product-dimensions -->';
		echo $op; // phpcs:ignore
	}

	public function template_tags() : array {
		return [
			'display_product_dimensions' => [ $this, 'display_product_dimensions' ],
		];
	}
}
