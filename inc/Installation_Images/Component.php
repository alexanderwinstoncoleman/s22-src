<?php
/**
 * WP_Rig\WP_Rig\Installation_Images\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Installation_Images;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;

/**
 * Class for managing custom related products.
 *
 * @link
 */
class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'installation_images';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		// add_filter( 'the_content', [ $this, 'get_acf_fields' ] );
		// add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_related_product_styles' ] );
	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_slick_slider_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}
		// wp_enqueue_style(
		// 	'wp-rig-product-slick-style',
		// 	get_theme_file_uri( '/assets/css/slick.min.css' )
		// );

		// wp_enqueue_script(
		// 	'wp-rig-product-slick',
		// 	get_theme_file_uri( '/assets/js/slick.min.js' ),
		// 	[],
		// 	wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/slick.min.js' ) ),
		// 	true
		// );
	}

	public function get_installation_images() {
		$image = '';
		$number = count( get_field( 'installation_images' ) );
		if ( have_rows( 'installation_images' ) ) :

			while ( have_rows( 'installation_images' ) ) : the_row();
				$img_id = get_sub_field( 'image' );
				$image .= '<div class="single-image">';
				$image .= wp_get_attachment_image( $img_id, 's22-installations-thumb' );
				$image .= '</div>';

			endwhile;

		endif;
		return $image;
	}

	public function display_installation_images() {
		// $op .= '<div class="installation-images col col-12">';
		$op .= $this->get_installation_images();
		// $op .= '</div><!-- .installation-images -->';
		echo $op;
	}

	public function template_tags() : array {
		return [
			'display_installation_images' => [ $this, 'display_installation_images' ],
		];
	}
}
