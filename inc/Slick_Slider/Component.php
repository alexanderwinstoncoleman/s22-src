<?php
/**
 * WP_Rig\WP_Rig\Slick_Slider\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Slick_Slider;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use WP_Post;
use function add_action;
use function add_filter;
use function wp_enqueue_script;
use function wp_enqueue_style;
use function get_theme_file_uri;
use function get_theme_file_path;
use function wp_script_add_data;
use function wp_localize_script;

/**
 * Class for improving accessibility among various core features.
 */
class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'slick_slider';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_slick_slider_script' ] );
	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_slick_slider_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}

		// Enqueue the navigation script.
//		wp_enqueue_script(
//			'wp-rig-navigation',
//			get_theme_file_uri( '/assets/js/navigation.min.js' ),
//			[],
//			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/navigation.min.js' ) ),
//			false
//		);
		// Putting this here because it didn't work anywhere else...
		wp_enqueue_style(
			'wp-rig-slick-style',
			get_theme_file_uri( '/assets/css/slick.min.css' )
		);

		wp_enqueue_script(
			'wp-rig-slick',
			get_theme_file_uri( '/assets/js/slick.min.js' ),
			array( 'jquery' ),
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/slick.min.js' ) ),
			true
		);
		wp_script_add_data( 'wp-rig-slick', 'async', true );

		wp_localize_script(
			'wp-rig-navigation',
			'wpRigScreenReaderText',
			[
				'expand'   => __( 'Expand child menu', 'wp-rig' ),
				'collapse' => __( 'Collapse child menu', 'wp-rig' ),
			]
		);
	}

	public function display_front_top_image() {
		$image_id  = get_field( 'top_image' );
		$image     = wp_get_attachment_image_url( $image_id, 'full' );
		$tagline_1 = get_field( 'tagline_1' );
		$tagline_2 = get_field( 'tagline_2' );
		$css = 'style="background-image:url(' . $image . ')"';
		// var_dump( $image_id );
		$op = '';
		$op .= '<div id="front-top-container" class="front-top-container" ' . $css . '>';
		$op .= '<div class="tagline">';
		$op .= '<div class="grid-container">';
		$op .= '<div class="col col-12">';
		$op .= '<h2>' . esc_html( $tagline_1 ) . '</h2>';
		$op .= '<h2>' . esc_html( $tagline_2 ) . '</h2>';
		$op .= '</div>';
		$op .= '</div>';
		$op .= '</div> <!-- tagline -->';
		$op .= '</div>';
		echo $op;
	}

	public function display_slick_slider(){
		$one = get_stylesheet_directory_uri() . '/assets/images/DELETE_slider1.jpg';
		$two = get_stylesheet_directory_uri() . '/assets/images/DELETE_slider2.jpg';
		$three = get_stylesheet_directory_uri() . '/assets/images/DELETE_slider3.jpg';
		$slider = ' <div id="front-top-container" class="front-top-container">
						<div class="carousel slick-front-top">
							<div style="background-image:url(' . $three . ');"></div>
							<div style="background-image:url(' . $two . ');"></div>
							<div style="background-image:url(' . $one . ');"></div>
						</div>
						<div class="tagline">
							<div class="grid-container">
								<div class="col col-12">
									<h2>Italian opulence.</h2>
									<h2>Superior comfort.</h2>
								</div>
							</div>
						</div>
					</div>';
		echo $slider;
	}
	public function template_tags() : array {
		return [
			'display_slick_slider' => [ $this, 'display_slick_slider' ],
			'display_front_top_image' => [ $this, 'display_front_top_image' ],
		];
	}
}
