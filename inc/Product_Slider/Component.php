<?php
/**
 * WP_Rig\WP_Rig\Product_Slider\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Product_Slider;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;

/**
 * Class for managing custom related products.
 *
 * @link
 */
class Component implements Component_Interface, Templating_Component_Interface {
	public $product_array = [];
	public $product_block;
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'product_slider';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		// add_filter( 'the_content', [ $this, 'get_acf_fields' ] );
		// add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_related_product_styles' ] );
	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_slick_slider_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}
		wp_enqueue_style(
			'wp-rig-product-slick-style',
			get_theme_file_uri( '/assets/css/slick.min.css' )
		);

		wp_enqueue_script(
			'wp-rig-product-slick',
			get_theme_file_uri( '/assets/js/slick.min.js' ),
			[],
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/slick.min.js' ) ),
			true
		);
	}

	public function get_slider_images() {
		$post_type = get_post_type();

		if ( ! is_singular( $post_type ) ) {
			return;
		}
		$image = '';
		if ( 's22_product' === $post_type ) {
			$row = 'gallery';
			$sub = 'gallery_image';
			$img_size = 'wp-rig-featured';
		}
		if ( 'installation_post' === $post_type ) {
			$row = 'installation_images';
			$sub = 'image';
			$img_size = 's22-installations-featured';
		}
		if ( have_rows( $row ) ) :

			while ( have_rows( $row ) ) : the_row();
				$img_id = get_sub_field( $sub );
				$image .= '<div class="single-image cell">';
				$image .= wp_get_attachment_image( $img_id, $img_size );
				$image .= '</div>';

			endwhile;

		endif;
		return $image;
	}

	public function display_product_slider() {
		$op .= '<div class="product-slider carousel">';
		$op .= $this->get_slider_images();
		$op .= '</div><!-- .product-slider -->';
		echo $op;
	}

	public function template_tags() : array {
		return [
			'display_product_slider' => [ $this, 'display_product_slider' ],
		];
	}
}
