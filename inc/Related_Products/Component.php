<?php
/**
 * WP_Rig\WP_Rig\Related_Products\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Related_Products;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;

/**
 * Class for managing custom related products.
 *
 * @link
 */
class Component implements Component_Interface, Templating_Component_Interface {
	public $product_array = [];
	public $product_block;
	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'related_products';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {
		// add_filter( 'the_content', [ $this, 'get_acf_fields' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_related_product_styles' ] );
	}

	public function action_enqueue_related_product_styles() {
		wp_enqueue_style(
			'wp-rig-related-style',
			get_theme_file_uri( '/assets/css/related-product.min.css' )
		);
	}

	/**
	 * Get custom featured products.
	 */
	public function display_related_products() {
		$suffix = get_post_type();
		$slider_or_grid_classes = ' related-' . $suffix;
		$column_class = '';
		if( is_singular( 's22_product' ) ) {
			$product_choice = get_field( 'product_choice' );
			$section_title  = 'Related Products';
			$img_size       = 's22-product-small';
		}
		if( is_singular( 'installation_post' ) ) {
			$product_choice = get_field( 'installation_product_choice' );
			// var_dump($product_choice);
			$section_title  = 'Products Used';
			$img_size       = 's22-product-small';
			// $slider_or_grid_classes = ' installation-slider';
		}
		if ( ! $product_choice && ( ! is_singular( 's22_product' ) || ! is_singular( 'installation_post' ) ) ) {
			return;
		}
		$post_count = count( $product_choice );
		if ( 4 < $post_count ) {
			// if more than 4 products, insert slider class
			$slider_or_grid_classes .= ' carousel';
		} else {
			$slider_or_grid_classes .= ' grid-container';
		}

		// $column_class = $this->get_column_count( $post_count );
		$column_class = '';
		$this->product_block .= '<div class="related-products-container text-center">';
		$this->product_block .= '<h3>' . $section_title . '</h3>';
		$this->product_block .= '<div class="related-products' . $slider_or_grid_classes . '">';
		// var_dump( count( $product_choice ) );
		foreach ( $product_choice as $key => $product ) {
			// var_dump( wp_get_attachment_metadata( $product ) );
			$this->product_block .= '<div class="related-product' . $column_class . '">';
			$this->product_block .= '<a href="' . get_the_permalink( $product ) . '">';
			$this->product_block .= get_the_post_thumbnail( $product, $img_size, array( 'class' => $suffix . '-img related-img' ) );
			$this->product_block .= '<h2>' . get_the_title( $product ) . '</h2>';
			$this->product_block .= '</a>';
			$this->product_block .= '</div>';
		}
		$this->product_block .= '</div>';
		$this->product_block .= '</div>';
		// return $content . $this->product_block;
		echo $this->product_block;
	}

	/**
	 * Return column classes if there is no slider
	 */
	private function get_column_count( $post_count ) {
		switch ($post_count) {
			case 4:
				$column_class = ' col col-3';
				break;

			case 3:
				$column_class = ' col col-4';
				break;

			case 2:
				# code...
				$column_class = ' col col-3 push-3';
				break;

			case 1:
				# code...
				$column_class = ' col col-3 push-4';
				break;

			default:
				# code...
				$column_class = ' col';
				break;
		}
		return $column_class;
	}

	public function template_tags() : array {
		return [
			'display_related_products' => [ $this, 'display_related_products' ],
		];
	}
}
