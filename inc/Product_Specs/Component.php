<?php
/**
 * WP_Rig\WP_Rig\Product_Specs\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Product_Specs;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;

/**
 * Class for managing product specs.
 *
 * @link
 */

class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'product_specs';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {

	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_slick_slider_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() ) {
			return;
		}
	}

	public function file_button( $text, $file_id = false ) {
		if ( ! $text && ! $file_id ) {
			return;
		}
		if ( ! $text ) {
			$text = 'View';
		}
		$file_url = wp_get_attachment_url( $file_id );
		$op  = '';
		$op .= '<a href="' . $file_url . '" target="_blank" class="button">';
		$op .= $text;
		$op .= '</a>';
		return $op;
	}

	public function display_product_specs() {
		$op = '';
		$repeater = get_field( 'section' );
		// There is always one row in this repeater.
		// Checking if there is an array, OR if first row is there by default and therefore empty
		$single_row_empty = ( ! is_array( $repeater[0] ) || ( 1 === count( $repeater ) ) && 0 === count( array_filter( $repeater[0] ) ) );
		if ( $single_row_empty ) {
			return;
		}

		// if( 1 === count( $repeater) && $repeater)
		if ( have_rows( 'section' ) ) {
			$op .= '<div class="product-specs">';
			while ( have_rows( 'section' ) ) {
				the_row();
				$button_text = get_sub_field( 'button_text' );
				$file_id = get_sub_field( 'file_upload' );
				$additional_content = get_sub_field( 'additional_content' );
				$op .= '<div class="spec-details">';
				$op .= '<p class="spec-title bold">' . get_sub_field( 'section_title' ) . ':</p>';
				if ( $file_id ) {
					$op .= $this->file_button( $button_text, $file_id );
				}
				if ( $additional_content ) {
					$op .= $additional_content;
				}
				$op .= '</div>';
			}
			$op .= '</div><!-- .product-specs -->';
		}

		echo $op;
	}

	public function template_tags() : array {
		return [
			'display_product_specs' => [ $this, 'display_product_specs' ],
		];
	}
}
