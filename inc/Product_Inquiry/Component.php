<?php
/**
 * WP_Rig\WP_Rig\Product_Inquiry\Component class
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig\Product_Inquiry;

use WP_Rig\WP_Rig\Component_Interface;
use WP_Rig\WP_Rig\Templating_Component_Interface;
use function WP_Rig\WP_Rig\wp_rig;
use function add_filter;
use function wp_enqueue_style;
use function the_ID;
use function wp_footer;

/**
 * Class for managing product inquiry form.
 *
 * @link
 */

class Component implements Component_Interface, Templating_Component_Interface {

	/**
	 * Gets the unique identifier for the theme component.
	 *
	 * @return string Component slug.
	 */
	public function get_slug() : string {
		return 'product_inquiry';
	}

	/**
	 * Adds the action and filter hooks to integrate with WordPress.
	 */
	public function initialize() {

		// add_action( 'wp_enqueue_scripts', [ $this, 'action_enqueue_caldera_modal_script' ] );
		// add_action( 'wp_footer', [ $this, 'display_product_inquiry_form' ] );
	}

	/**
	 * Enqueues a Slick script.
	 */
	public function action_enqueue_caldera_modal_script() {

		// If the AMP plugin is active, return early.
		if ( wp_rig()->is_amp() || ! is_singular( 's22_product' ) ) {
			return;
		}

		wp_enqueue_script(
			'wp-rig-caldera-modal',
			get_theme_file_uri( '/assets/js/caldera.min.js' ),
			[],
			wp_rig()->get_asset_version( get_theme_file_path( '/assets/js/caldera.min.js' ) ),
			false
		);

		wp_script_add_data( 'wp-rig-caldera-modal', 'defer', true );
	}

	public function display_product_inquiry_form() {
		$form = '';

		$form .= '<div id="form-inquiry-container" class="form-inquiry-container">';
		$form .= '<div class="form-inner">';
		$form .= '<a href="#" id="form_close" class="form_close"><i class="fal fa-times-circle"></i></a>';
		$form .= '<p>Product: ' . get_the_title() . '</p>';
		$form .= get_the_post_thumbnail( get_the_ID(), 's22-product-small' );
		$form .= do_shortcode( '[caldera_form id="CF5db8f73c05fdf"]' );
		$form .= '</div>';
		$form .= '</div>';

		echo $form;
	}

	public function display_product_inquiry_button() {
		$id = get_the_ID();
		$subject = 'Product Inquiry: ' . get_the_title($id);
		$content = 'Regarding: ' . get_the_permalink( $id );
		$button  = '<p class="product-inquiry-container">';
		// $button .= '<a href="#" id="form_launch" class="button form_launch">Inquire</a>';
		$button .= '<a href="mailto:roberto@suite22contract.com?subject=' . $subject . '&body=' . $content . '" id="form_launch" class="button form_launch" target="_blank">Inquire</a>';
		$button .= '</p>';
		echo $button;
	}

	public function template_tags() : array {
		return [
			'display_product_inquiry_form'   => [ $this, 'display_product_inquiry_form' ],
			'display_product_inquiry_button' => [ $this, 'display_product_inquiry_button' ],
		];
	}
}
