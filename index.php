<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

get_header();

wp_rig()->print_styles( 'wp-rig-content' );

$grid_classes = 'grid-container';
if ( is_search() ) {
	$grid_classes .= ' archive-grid search-grid';
}

?>
	<main id="primary" class="site-main">
		<div class="<?php echo esc_html( $grid_classes ); ?>">
			<?php
			if ( have_posts() ) {

				get_template_part( 'template-parts/content/page_header' );

				while ( have_posts() ) {
					the_post();

					get_template_part( 'template-parts/content/entry', get_post_type() );
				}

				if ( ! is_singular() ) {
					get_template_part( 'template-parts/content/pagination' );
				}
			} else {
				get_template_part( 'template-parts/content/error' );
			}
			?>
		</div><!-- .grid-container -->
	</main><!-- #primary -->
<?php
// get_sidebar();
get_footer();
