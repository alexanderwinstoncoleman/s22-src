/**
 * Lazy-load images script.
 *
 * @link https://developers.google.com/web/fundamentals/performance/lazy-loading-guidance/images-and-video/
 */
document.addEventListener( 'DOMContentLoaded', function() {
	let lazyImages = [].slice.call( document.querySelectorAll( '.lazy' ) );
	let videoPaused = false;
	const introVideo = document.getElementById( 'introVideo' );
	if ( 'IntersectionObserver' in window ) {
		const lazyImageObserver = new IntersectionObserver( function( entries ) {
			entries.forEach( function( entry ) {
				const isVideo = 'VIDEO' === entry.target.tagName;
				const lazyImage = entry.target;
				if ( entry.isIntersecting ) {
					// console.log( entry );
					if ( lazyImage.dataset.src ) {
						lazyImage.src = lazyImage.dataset.src;
					}

					if ( isVideo ) {
						lazyImage.dataset.src = '';
					}
					// console.log( entry.target );
					if ( lazyImage.dataset.srcset ) {
						lazyImage.srcset = lazyImage.dataset.srcset;
					}
					if ( lazyImage.dataset.sizes ) {
						lazyImage.sizes = lazyImage.dataset.sizes;
					}
					if (
						isVideo &&
						videoPaused &&
						lazyImage.src &&
						lazyImage.id === introVideo.id
					) {
						// lazyImage.play();
						introVideo.play();
					}
					lazyImage.classList.remove( 'lazy' );
					if ( ! isVideo ) {
						lazyImageObserver.unobserve( lazyImage );
					}
				} else if ( ! entry.isIntersecting && isVideo ) {
					videoPaused = true;
					if ( lazyImage.id === introVideo.id ) {
						introVideo.pause();
					}
				}
			} );
		} );

		lazyImages.forEach( function( lazyImage ) {
			lazyImageObserver.observe( lazyImage );
		} );
	} else {
		// For older browsers lacking IntersectionObserver support.
		// See https://developers.google.com/web/fundamentals/performance/lazy-loading-guidance/images-and-video/
		let active = false;

		const lazyLoad = function() {
			if ( false === active ) {
				active = true;

				setTimeout( function() {
					lazyImages.forEach( function( lazyImage ) {
						if (
							lazyImage.getBoundingClientRect().top <=
								window.innerHeight &&
							0 <= lazyImage.getBoundingClientRect().bottom &&
							'none' !== getComputedStyle( lazyImage ).display
						) {
							lazyImage.src = lazyImage.dataset.src;
							if ( lazyImage.dataset.srcset ) {
								lazyImage.srcset = lazyImage.dataset.srcset;
							}
							if ( lazyImage.dataset.sizes ) {
								lazyImage.sizes = lazyImage.dataset.sizes;
							}
							lazyImage.classList.remove( 'lazy' );

							lazyImages = lazyImages.filter( function( image ) {
								return image !== lazyImage;
							} );

							if ( 0 === lazyImages.length ) {
								document.removeEventListener(
									'scroll',
									lazyLoad
								);
								window.removeEventListener( 'resize', lazyLoad );
								window.removeEventListener(
									'orientationchange',
									lazyLoad
								);
							}
						}
					} );

					active = false;
				}, 200 );
			}
		};

		document.addEventListener( 'scroll', lazyLoad );
		window.addEventListener( 'resize', lazyLoad );
		window.addEventListener( 'orientationchange', lazyLoad );
	}
	// LAZY LOAD VIDEO????
	// const lazyVideos = [].slice.call( document.querySelectorAll( 'video' ) );
	// if ( 'IntersectionObserver' in window ) {
	// 	const lazyVideoObserver = new IntersectionObserver( function(
	// 		entries,
	// 		observer
	// 	) {
	// 		entries.forEach( function( video ) {
	// 			if ( video.isIntersecting ) {
	// 				video.target.play();
	// 			} else {
	// 				video.target.pause();
	// 			}
	// 		} );
	// 	} );

	// 	lazyVideos.forEach( function( lazyVideo ) {
	// 		lazyVideoObserver.observe( lazyVideo );
	// 	} );
	// }
} );
