const button = document.getElementById( 'form_launch' );
const form = document.getElementById( 'form-inquiry-container' );
const closeButton = document.getElementById( 'form_close' );

button.addEventListener( 'click', openForm );
closeButton.addEventListener( 'click', closeForm );

function openForm( e ) {
	e.preventDefault();
	form.classList.add( 'open' );
}
function closeForm( e ) {
	e.preventDefault();
	form.classList.remove( 'open' );
}

function afterFormSubmit( obj ) {
	if ( 'complete' == obj.status ) {
		setTimeout( () => {
			form.classList.remove( 'open' );
		}, 1500 );
	}
}
