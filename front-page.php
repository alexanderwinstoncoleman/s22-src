<?php
/**
 * Render your site front page, whether the front page displays the blog posts index or a static page.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#front-page-display
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

use acf_field_repeater as acf_field_repeaterAlias;

get_header();

// Use grid layout if blog index is displayed.
if ( is_front_page() ) {
	wp_rig()->print_styles( 'wp-rig-content', 'wp-rig-front-page' );
} else {
	wp_rig()->print_styles( 'wp-rig-content' );
}

wp_rig()->display_front_top_image();
?>
	<div class="video-container">
		<video autoplay muted loop playsinline id="introVideo" class="lazy" preload="true" data-src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/video/s22-small_2.mp4' ); ?>" poster="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/images/video-poster2.jpg' ); ?>">
		</video>
	</div>

	<main id="primary" class="site-main">
		<?php
		$align = '';
		$button_counter = 0;
		if ( have_rows( 'area' ) ) :
			while ( have_rows( 'area' ) ) :
				the_row();
				$layout = get_row_layout();
				if ( 0 === $button_counter % 2 ) {
					$align = 'text-right';
				} else {
					$align = '';
				}
				if ( 'button' === $layout ) {
					$button       = get_sub_field( 'button' );
					$img_url      = get_sub_field( 'button_image' )['url'];
					$title_text   = get_sub_field( 'button_title' );
					$text         = get_sub_field( 'button_text' );
					$url_choice   = get_sub_field( 'page_or_url' );
					$anchor_close = '</a>';
					if ( 'page' === $url_choice ) {
						$url         = get_sub_field( 'link_to_page' );
						$anchor_open = '<a href="' . esc_url( $url ) . '">';
					} elseif ( 'url' === $url_choice ) {
						$url         = get_sub_field( 'url' );
						$anchor_open = '<a href="' . esc_url( $url ) . '">';
					} else {
						$anchor_open  = '';
						$anchor_close = '';
					}
					include 'template-parts/front-page/front-buttons.php';
					$button_counter++;
				}

				if ( 'product_slider' === $layout ) {
					get_template_part( 'template-parts/front-page/front-products' );
				}

				if ( 'installation_slider' === $layout ) {
					get_template_part( 'template-parts/front-page/front-installations' );
				}
			endwhile;
		endif;
		?>
	</main><!-- #primary -->
	<div class="insta-container">
		<?php echo do_shortcode( '[instagram-feed width=100 widthunit=%]' ); ?>
	</div>
<?php
get_footer();
