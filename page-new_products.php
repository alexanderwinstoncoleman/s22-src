<?php
/**
 * Template Name: New Products
 *
 * When active, by adding the heading above and providing a custom name
 * this template becomes available in a drop-down panel in the editor.
 *
 * Filename can be anything.
 *
 * @link https://developer.wordpress.org/themes/template-files-section/page-template-files/#creating-custom-page-templates-for-global-use
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

get_header();

wp_rig()->print_styles( 'wp-rig-content' );

?>
	<main id="primary" class="site-main">
		<div class="grid-container">
			<div class="col col-12 text-center">
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header>
			</div>
			<?php
			$paged = ( get_query_var('page') ) ? get_query_var('page') : 1;

			$args = array(
				'post_type' => 's22_product',
				'paged'     => $paged,
			);

			$q = new \WP_Query( $args );

			while ( $q->have_posts() ) {
				$q->the_post();

				get_template_part( 'template-parts/content/entry', get_post_type() );
			}
			get_template_part( 'template-parts/content/pagination' );
			?>
		</div>
	</main><!-- #primary -->
<?php
// get_sidebar();
get_footer();
