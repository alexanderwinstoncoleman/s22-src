<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>

			<footer id="colophon" class="site-footer">
				<?php get_template_part( 'template-parts/footer/widgets' ); ?>
				<div class="copyright">
					<div class="grid-container">
						<div class="col col-12">&copy; <?php echo date( 'Y' ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?></div>
					</div>
				</div>
			</footer><!-- #colophon -->
		</div><!-- #page -->
	</div><!-- #mp-pusher -->


	<?php
	wp_footer();
	if ( is_singular( 's22_product' ) ) {
		wp_rig()->display_product_inquiry_form();
	}
	?>
</body>
</html>
