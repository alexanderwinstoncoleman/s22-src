<?php
/**
 * Template part for displaying the header navigation open/close button
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

if ( ! wp_rig()->is_primary_nav_menu_active() ) {
	return;
}

?>

<a href="#" id="trigger" class="menu-trigger">Open/Close Menu</a>
