<?php
/**
 * Template part for displaying the menu toggle button
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

if ( ! wp_rig()->is_primary_nav_menu_active() ) {
	return;
}

?>

<div id="menu-toggle" class="menu-toggle-container menu-toggle">
		<?php // echo file_get_contents( get_stylesheet_directory_uri() . '/assets/images/magnifying-glass.svg' ); ?>
		<?php get_search_form(); ?>
	<button id="trigger" class="menu-trigger hamburger hamburger--slider" type="button">
	<span class="hamburger-box">
		<span class="hamburger-inner"></span>
	</span>
		<span id="menu-text" class="menu-text">Menu</span>
	</button>
</div>

<?php get_template_part( 'template-parts/header/navigation' ); ?>
