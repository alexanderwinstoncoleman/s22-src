<?php
/**
 * Template part for displaying the header navigation menu
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

if ( ! wp_rig()->is_primary_nav_menu_active() ) {
	return;
}

?>
<div class="nav-container">
	<nav id="mp-menu" class="mp-menu main-navigation nav--toggle-sub nav--toggle-small" aria-label="<?php esc_attr_e( 'Main menu', 'wp-rig' ); ?>"
		<?php
		if ( wp_rig()->is_amp() ) {
			?>
			[class]=" siteNavigationMenu.expanded ? 'main-navigation nav--toggle-sub nav--toggle-small nav--toggled-on' : 'main-navigation nav--toggle-sub nav--toggle-small' "
			<?php
		}
		?>
	>
		<?php
		if ( wp_rig()->is_amp() ) {
			?>
			<amp-state id="siteNavigationMenu">
				<script type="application/json">
					{
						"expanded": false
					}
				</script>
			</amp-state>
			<?php
		}
		?>

		<div class="primary-menu-container mp-level" data-level="1">
			<?php // wp_rig()->display_primary_nav_menu( [ 'menu_id' => 'primary' ] ); ?>
			<?php
				wp_nav_menu(
					array(
						'container' => false,
						'menu'      => 'Primary Menu',
						'walker'    => new \S22_Walker(),
					)
				);
			?>
		</div>
	</nav><!-- #site-navigation -->
</div>
