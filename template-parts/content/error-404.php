<?php
/**
 * Template part for displaying the page content when a 404 error has occurred
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

?>
<section class="error grid-container">
	<?php get_template_part( 'template-parts/content/page_header' ); ?>

	<div class="page-content col col-12 text-center">
		<p>
			<strong><?php esc_html_e( 'It looks like nothing was found here. Try searching below, or use our contact information.', 'wp-rig' ); ?></strong>
		</p>
		<div class="textwidget"><p>Suite 22 Contract Inc.<br>
			160 Bullock Dr.<br>
			Markham, ON<br>
			L3P 1W2<br>
			Canada</p>
			<p><strong>E: <a href="mailto:info@suite22contract.com">info@suite22contract.com</a></strong><br>
			<strong>P: <a href="tel:12895541500">1 289 554 1500</a></strong><br>
			<strong>F: 1 905 554 6783</strong></p>
		</div>
	</div><!-- .page-content -->
	<div class="col col-6 text-center offset-3">
	<?php
		get_search_form();

	?>
		<div class="text-center"><img class="text-center" src="<?php echo get_stylesheet_directory_uri() . '/assets/images/suite-22-logo-small.png'; ?>" alt="toronto's best italian furniture seller"></div>
	</div>
</section><!-- .error -->
