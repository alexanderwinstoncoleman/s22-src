<?php
/**
 * Template part for displaying the page header of the currently displayed page
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;
?>
	<?php
	if ( is_404() ) {
		?>
		<header class="page-header col col-12">
			<h1 class="page-title text-center">
				<?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'wp-rig' ); ?>
			</h1>
		</header><!-- .page-header -->
		<?php
	} elseif ( is_home() && ! have_posts() ) {
		?>
		<header class="page-header">
			<h1 class="page-title text-center">
				<?php esc_html_e( 'Nothing Found', 'wp-rig' ); ?>
			</h1>
		</header><!-- .page-header -->
		<?php
	} elseif ( is_home() && ! is_front_page() ) {
		?>
		<header class="page-header">
			<h1 class="page-title text-center">
				<?php single_post_title(); ?>
			</h1>
		</header><!-- .page-header -->
		<?php
	} elseif ( is_search() ) {
		?>
		<header class="page-header col col-12">
			<h1 class="page-title text-center">
				<?php
				printf(
					/* translators: %s: search query */
					esc_html__( 'Search Results for: %s', 'wp-rig' ),
					'<span>' . get_search_query() . '</span>'
				);
				?>
			</h1>
		</header><!-- .page-header -->
		<?php
	} elseif ( is_post_type_archive( 'installation_post' ) ) {
		?>
		<header class="page-header">
			<h1 class="page-title text-center">Installations</h1>
		</header>
		<?php
	} elseif ( is_archive() ) {
		?>
		<header class="page-header">
			<?php
			if ( get_the_archive_title() ) {
				the_archive_title( '<h1 class="page-title text-center">', '</h1>' );
			} elseif ( 's22_product' === get_post_type() || ! get_the_archive_title() ) {
				echo '<h1 class="page-title text-center">New Products</h1>';
			}
			?>
		</header><!-- .page-header -->
		<?php
	}
?>
