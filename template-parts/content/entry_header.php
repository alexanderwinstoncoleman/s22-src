<?php
/**
 * Template part for displaying a post's header
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

if ( is_singular() ) {
	$classes = ' col col-12';
} elseif ( is_archive() ) {
	$classes = ' archive-list-header';
} else {
	$classes = '';
}
?>

<header class="entry-header<?php echo esc_html( $classes ); ?>">
	<?php

	// get_template_part( 'template-parts/content/entry_meta', get_post_type() );
	if ( ( ! is_search() && ! is_singular( 's22_product' ) ) || is_search() ) {
		get_template_part( 'template-parts/content/entry_thumbnail', get_post_type() );
	}
	get_template_part( 'template-parts/content/entry_title', get_post_type() );
	?>
</header><!-- .entry-header -->

<?php

if ( is_singular( 's22_product' ) ) {
	?>
	<div class="s22_product-images col col-6">
	<?php
		get_template_part( 'template-parts/content/entry_thumbnail', get_post_type() );
	?>
	</div>
	<?php
}

if ( is_singular( 'installation_post' ) ) {
	?>
	<div class="col col-12">
		<div class="block">
		<?php
		wp_rig()->display_product_slider();
		?>
		</div>
	</div>
	<?php
}
