<?php
/**
 * Template part for displaying a post
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

$classes = 'entry';
if ( is_search() || is_archive() ) {
	$classes = $classes;
} else {
	$classes .= ' grid-container';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class( $classes ); ?>>
	<?php
	get_template_part( 'template-parts/content/entry_header', get_post_type() );
	if ( is_search() ) {
		get_template_part( 'template-parts/content/entry_summary', get_post_type() );
	} elseif ( ! is_archive() ) {
		get_template_part( 'template-parts/content/entry_content', get_post_type() );
	}
	?>
</article><!-- #post-<?php the_ID(); ?> -->

<?php
if ( is_single() ) {
	wp_rig()->display_related_products();
}
