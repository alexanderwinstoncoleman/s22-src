<?php
/**
 * Template part for displaying a post's featured image
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;

// Audio or video attachments can have featured images, so they need to be specifically checked.
$support_slug = get_post_type();
if ( 'attachment' === $support_slug ) {
	if ( wp_attachment_is( 'audio' ) ) {
		$support_slug .= ':audio';
	} elseif ( wp_attachment_is( 'video' ) ) {
		$support_slug .= ':video';
	}
}

if ( post_password_required() || ! post_type_supports( $support_slug, 'thumbnail' ) || ! has_post_thumbnail() || is_singular( 'installation_post' ) ) {
	return;
}

if ( is_singular( 's22_product' ) ) {
	wp_rig()->display_product_slider();
} elseif ( is_singular( get_post_type() ) ) {
	?>
	<div class="post-thumbnail">
		<?php
		if ( is_singular( 'installation_post' ) ) {
			$img_size = 's22-installations-featured';
		} else {
			$img_size = 'wp-rig-featured';
		}
			the_post_thumbnail( $img_size, [ 'class' => 'skip-lazy' ] );
		?>
	</div><!-- .post-thumbnail -->
	<?php
} else {
	?>
	<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true">
		<?php
		global $wp_query;
		if ( is_post_type_archive( 'installation_post') ) {
			$img_size = 's22-installations-thumb';
		} else {
			$img_size = 'post_thumbnail';
		}
		if ( 0 === $wp_query->current_post ) {
			the_post_thumbnail(
				$img_size,
				[
					'class' => 'skip-lazy',
					'alt'   => the_title_attribute(
						[
							'echo' => false,
						]
					),
				]
			);
		} else {
			the_post_thumbnail(
				$img_size,
				[
					'alt' => the_title_attribute(
						[
							'echo' => false,
						]
					),
				]
			);
		}
		?>
	</a><!-- .post-thumbnail!!! -->
	<?php
}
