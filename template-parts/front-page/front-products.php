<div class="slider-container product-slider">
	<div class="grid-container text-center">
		<h2 class="col-12">New Products</h2>
	</div>
	<div class="front-products carousel">
			<?php
			$args = array(
				'post_type'      => 's22_product',
				'posts_per_page' => 12,
			);
			$counter = 0;
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
					$url       = get_the_permalink();
					$thumb_url = get_the_post_thumbnail_url( get_the_ID(), 's22-product-small' );
					$title     = get_the_title();
					$data_or_src = 'data-lazy="' . $thumb_url . '"';
					echo '<div class="single-product">';
					echo '<a href="' . esc_html( $url ) . '" class="single-product">';
					echo '<img ' . $data_or_src . ' alt="' . esc_html( $title ) . '" width="280" height="280">';
					echo '<h2 class="text-center">' . esc_html( $title ) . '</h2>';
					echo '</a>';
					echo '</div>';
					$counter++;
				endwhile;
			} else {
				echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
	</div><!--/.front-products-->
	<div class="grid-container text-center">
		<p class="col-12"><a href="<?php echo get_post_type_archive_link( 's22_product' ); ?>">All New Products</a></p>
	</div>
</div>
