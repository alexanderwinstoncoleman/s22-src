 <?php
/**
 * Template for front page buttons
 *
 * @package s22
 */

?>

<?php echo $anchor_open; // phpcs:ignore ?>
<div class="front-button <?php echo $align; // phpcs:ignore ?>" style="background-image:url(<?php echo esc_html( $img_url ); ?>)">
		<div class="grid-container">
			<div class="col col-m-6 col-12">
				<h2><?php echo esc_html( $title_text ); ?></h2>
				<p><?php echo esc_html( $text ); ?></p>
				<div class="button-mask"></div>
			</div>
		</div>
</div>
<?php echo $anchor_close; // phpcs:ignore ?>
