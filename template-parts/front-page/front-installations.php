<div class="slider-container">
	<div class="grid-container text-center">
		<h2 class="col col-12">Installations</h2>
	</div>
	<div class="grid-container installations-container">
		<div class="front-installations carousel">
			<?php
			$args = array(
				'post_type'      => 'installation_post',
				'posts_per_page' => 12,
			);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post();
					$url = get_the_permalink();
					$thumb_url = get_the_post_thumbnail_url( get_the_ID(), 's22-installations-thumb' );
					$data_or_src = 'data-lazy="' . $thumb_url . '"';
					$title = get_the_title();
					echo '<div class="single-product">';
					echo '<a href="' . $url . '" class="single-product">';
					echo '<img ' . $data_or_src . ' alt="' . esc_html( $title ) . '" width="280" height="280">';
					echo '<h2 class="text-center">' . $title . '</h2>';
					echo '</a>';
					echo '</div>';
				endwhile;
			} else {
				echo __( 'No products found' );
			}
			wp_reset_postdata();
			?>
		</div><!--/.front-products-->
	</div>
	<div class="grid-container text-center">
		<p class="col-12"><a href="<?php echo get_post_type_archive_link( 'installation_post' ); ?>">All Installations</a></p>
	</div>
</div>
