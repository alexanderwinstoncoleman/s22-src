<?php
/**
 * The sidebar containing the footer widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package wp_rig
 */

namespace WP_Rig\WP_Rig;
if ( ! wp_rig()->is_footer_widget_active() ) {
	return;
}

wp_rig()->print_styles( 'wp-rig-sidebar', 'wp-rig-widgets' );

?>
<div id="footer-widgets" class="footer-widgets widget-area grid-container">
	<?php wp_rig()->display_footer_widget(); ?>
</div><!-- #secondary -->
