# Suite 22 Contract Theme

### Built with WP Rig

Does not use SASS, but "modern" CSS (whatever "modern" CSS means! ¯\\\_(ツ)\_/¯ ). Nesting, custom properties (variables), imports, and other SASS-like options are available. The Gulp workflow/NPM packages take care of spitting out browser ready CSS until browsers adopt this model. So it is very reminiscent of SASS, and something I wanted to try out for this build. After working with this method, SASS still feels a bit more complete. Further projects will be built with SASS.

[Current CSS Nesting Module](https://drafts.csswg.org/css-nesting-1/)

#### Custom Components in `inc` directory:

1. `/Archive_Titles`
2. `/Related_Products`
3. `/Slick_Slider`
4. `/Svg_Logo`

## How to build production:

1. Follow the steps to [install WP Rig](https://github.com/wprig/wprig#installation).
2. Run `npm run bundle` from inside the development theme folder.
3. A new, production ready theme will be generated in `wp-content/themes`.
4. The production theme can be activated or uploaded to a production environment.

### Available Processes

#### `dev watch` process:

`npm run dev` will run the default development task that processes source files. While this process is running, source files will be watched for changes and the BrowserSync server will run. This process is optimized for speed so you can iterate quickly.

#### `dev build` process:

`npm run build` processes source files one-time. It does not watch for changes nor start the BrowserSync server.

#### `production bundle` process:

`npm run bundle` generates a production ready theme as a new theme directory and, optionally, a `.zip` archive. This builds all source files, optimizes the built files for production, does a string replacement and runs translations. Non-essential files from the development theme folder are not copied to the production theme.

### Gulp process

WP Rig uses a [Gulp 4](https://gulpjs.com/) build process to generate and optimize the code for the theme. All development is done in the development theme folder. Asset files (CSS, JavaScript and images) are processed by gulp in the following locations:

-   CSS: `assets/css/src`
-   JavaScript: `assets/js/src`
-   images: `assets/images/src`
